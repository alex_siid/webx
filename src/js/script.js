$("input[name=phone]").mask("+7 (999) 999-99-99");


$(window).scroll(function() {
    if ($(this).scrollTop() > 1600) {
        $('.page-up').fadeIn();
    } else {
    $('.page-up').fadeOut();
    }
});


$("a").click( function() {
    const _href = $(this).attr("href");
    $("html, body").animate({scrollTop: $(_href).offset().top+"px"});
    return false;
});

window.addEventListener('DOMContentLoaded', () => {
    const menu = document.querySelector('.menu'),
    menuItem = document.querySelectorAll('.menu_item'),
    hamburger = document.querySelector('.hamburger');

    hamburger.addEventListener('click', () => {
        hamburger.classList.toggle('hamburger_active');
        menu.classList.toggle('menu_active');
    });

    menuItem.forEach(item => {
        item.addEventListener('click', () => {
            hamburger.classList.toggle('hamburger_active');
            menu.classList.toggle('menu_active');
        });
    });

    $("a[href^='#description'], a[href^='#products'], a[href^='#team'], a[href^='#principles'], a[href^='#contacts']").click( function() {
        hamburger.classList.toggle('hamburger_active');
        menu.classList.toggle('menu_active');
    });
});


//Модальные окна
$('[data-modal=order]').on('click', function() {
    $('.overlay, #thanks').fadeOut('0.5s'); 
    $('.overlay, #order').fadeIn('0.5s');      
});
$('.btn_submit').on('click', function(e) {
    e.preventDefault();
    $('.overlay, #order').fadeOut('0s'); 
    $('.overlay, #thanks').fadeIn('0s'); 
});
$('.modal__close').on('click', function() {
    $('.overlay, #consultation, #order, #thanks').fadeOut('slow');
});